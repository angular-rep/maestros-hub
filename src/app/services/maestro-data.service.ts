import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MaestroDataService {
  private composers: string[] = [
    'A. R. Rahman',
    'Ilaiyaraaja',
    'Yuvan Shankar Raja',
    'Anirudh Ravichander',
    'Harris Jayaraj',
    'G. V. Prakash Kumar',
    'Santhosh Narayanan',
    'D. Imman',
    'M. M. Keeravani (M. M. Kreem)',
    'Sean Roldan',
    'Vidyasagar',
    'Deva',
    'Karthik Raja',
    'Bharathwaj',
    'S. A. Rajkumar',
    'Devi Sri Prasad',
    'V. Selvaganesh',
    'Shankar Mahadevan',
    'Ehsaan Noorani',
    'Loy Mendonsa',
    'S. Thaman',
    'Hiphop Tamizha (Adhi)',
    'Vivek-Mervin',
    'Sam C. S.',
    'Siddharth Vipin',
    'Vijay Antony',
    'M. Ghibran',
    'Simon K. King',
    'Justin Prabhakaran',
    'Nivas K. Prasanna',
    'Govind Vasantha',
    'Radhan',
  ];

  private bioList: ComposerBio[] = [
    {
      name: 'A. R. Rahman',
      bio: "A. R. Rahman is a legendary composer known for his work in Indian and international cinema. Born on January 6, 1967, in Chennai, India, as A. S. Dileep Kumar. Rahman's breakthrough came with the film Roja (1992), and he has since won numerous awards, including two Academy Awards. His unique fusion of Indian classical music with electronic music has earned him global recognition.",
    },
    {
      name: 'Ilaiyaraaja',
      bio: 'Ilaiyaraaja, born on June 2, 1943, is a prolific composer, conductor, and songwriter. He is one of the most respected figures in the Indian film music industry. Ilaiyaraaja has composed over 7,000 songs in various languages and is known for his versatility.',
    },
    {
      name: 'Yuvan Shankar Raja',
      bio: 'Yuvan Shankar Raja, born on August 31, 1979, is a composer and singer. He is the son of legendary composer Ilaiyaraaja. Yuvan is known for his work in Tamil, Telugu, and Malayalam cinema and is recognized for his innovative musical style.',
    },
    {
      name: 'Anirudh Ravichander',
      bio: 'Anirudh Ravichander, born on October 16, 1990, is a young and popular composer in the Tamil film industry. He gained widespread acclaim with the song Why This Kolaveri Di from the film 3 (2012). Anirudh is known for his fresh and modern approach to music.',
    },
    {
      name: 'Harris Jayaraj',
      bio: 'Harris Jayaraj, born on January 8, 1975, is a prominent composer in Tamil and Telugu cinema. He has delivered many chart-topping songs and is known for his melodious compositions. Harris started his career with Minnale (2001) and quickly rose to fame.',
    },
    {
      name: 'G. V. Prakash Kumar',
      bio: 'G. V. Prakash Kumar, born on June 13, 1987, is a composer, actor, and singer. He is the nephew of A. R. Rahman and has made a mark in Tamil cinema with his diverse musical style. G. V. Prakash Kumar started his career with Veyil (2006).',
    },
    {
      name: 'Santhosh Narayanan',
      bio: 'Santhosh Narayanan, born on May 15, 1983, is a composer and music director. He made his debut with the critically acclaimed film Attakathi (2012). Santhosh is known for his experimental and unique sound.',
    },
    {
      name: 'D. Imman',
      bio: 'D. Imman, born on January 24, 1983, is a composer who predominantly works in Tamil cinema. He has composed music for a wide range of films and has gained popularity for his catchy tunes. Imman has received several awards for his contributions to the industry.',
    },
    {
      name: 'M. M. Keeravani (M. M. Kreem)',
      bio: 'M. M. Keeravani, born on July 4, 1961, is a versatile composer who has worked in multiple Indian languages. He is known for his work in films like Baahubali and Magadheera. M. M. Keeravani has won several awards for his outstanding contributions to the music industry.',
    },

    {
      name: 'Sean Roldan',
      bio: 'Sean Roldan, born on May 25, 1988, is a composer and singer. He gained recognition with his work in films like Mundasupatti (2014) and Power Paandi (2017). Sean is known for his soulful and refreshing compositions.',
    },
  ];

  //prepare composers data from composers array
  getComposersData(): Composer[] {
    let composersData: Composer[] = [];
    let id = 1;
    this.composers.forEach((composer) => {
      let composerData: Composer = {
        id: id,
        name: composer,
        movies: [],
        songs: [],
      };
      composersData.push(composerData);
      id++;
    });
    return composersData;
  }

  getListOfComposers(): string[] {
    return this.composers;
  }

  getComposerData(name: string): Composer {
    let composerData: Composer = {
      id: 0,
      name: '',
      movies: [],
      songs: [],
    };
    this.getComposersData().forEach((composer) => {
      if (composer.name === name) {
        composerData = composer;
      }
    });
    return composerData;
  }

  getBioList(): ComposerBio[] {
    return this.bioList;
  }

  getComposerBio(name: string): string {
    let bio = '';
    this.bioList.forEach((composer) => {
      if (composer.name === name) {
        bio = composer.bio;
      }
    });
    return bio;
  }

  constructor() {}
}

export interface Composer {
  id: number;
  name: string;
  movies: string[];
  songs: string[];
  birthDate?: string;
}

export interface ComposerBio {
  name: string;
  bio: string;
}
// Path: src/app/services/maestro-data.service.ts
