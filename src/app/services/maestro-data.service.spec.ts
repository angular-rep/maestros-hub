import { TestBed } from '@angular/core/testing';

import { MaestroDataService } from './maestro-data.service';

describe('MaestroDataService', () => {
  let service: MaestroDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MaestroDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // test case for getComposersData()
  it('should return composers data', () => {
    let composersData = service.getComposersData();
    expect(composersData.length).toBe(32);
  });

  //test case for getListOfComposers()
  it('should return list of composers', () => {
    let composers = service.getListOfComposers();
    expect(composers.length).toBe(32);
  });

  //test case for getBioList()
  it('should return bio list', () => {
    let bioList = service.getBioList();
    expect(bioList.length).toBe(10);
  });

  //test case for getComposerBio()
  it('should return bio of composer', () => {
    let bio = service.getComposerBio('A. R. Rahman');
    expect(bio).toBe(
      "A. R. Rahman is a legendary composer known for his work in Indian and international cinema. Born on January 6, 1967, in Chennai, India, as A. S. Dileep Kumar. Rahman's breakthrough came with the film Roja (1992), and he has since won numerous awards, including two Academy Awards. His unique fusion of Indian classical music with electronic music has earned him global recognition."
    );
  });

  //test case for getComposerBio()
  it('should return empty string if composer not found', () => {
    let bio = service.getComposerBio('A. R. Raman');
    expect(bio).toBe('');
  });

  //test case for getComposerData()
  it('should return composer data', () => {
    let composerData = service.getComposerData('A. R. Rahman');
    expect(composerData?.name).toBe('A. R. Rahman');
  });

  //test case for getComposerData()
  it('should return undefined if composer not found', () => {
    let composerData = service.getComposerData('A. R. fsdf');
    expect(composerData.id).toBe(0);
  });
});
