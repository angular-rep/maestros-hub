import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaestroCardComponent } from './maestro-card.component';

describe('MaestroCardComponent', () => {
  let component: MaestroCardComponent;
  let fixture: ComponentFixture<MaestroCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MaestroCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MaestroCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
