import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Composer, ComposerBio } from '../services/maestro-data.service';

@Component({
  selector: 'app-maestro-card',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './maestro-card.component.html',
  styleUrl: './maestro-card.component.scss',
})
export class MaestroCardComponent {
  @Input({ required: true }) composer: Composer | undefined = undefined;
  @Input({ required: true }) composerBio: string = '';

  constructor() {}
}
