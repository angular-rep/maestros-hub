import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaestroContainerComponent } from './maestro-container.component';

describe('MaestroContainerComponent', () => {
  let component: MaestroContainerComponent;
  let fixture: ComponentFixture<MaestroContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MaestroContainerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MaestroContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
