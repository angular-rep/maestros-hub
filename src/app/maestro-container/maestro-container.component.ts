import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaestroCardComponent } from '../maestro-card/maestro-card.component';
import { MaestroListComponent } from '../maestro-list/maestro-list.component';
import { Composer, MaestroDataService } from '../services/maestro-data.service';

@Component({
  selector: 'app-maestro-container',
  standalone: true,
  templateUrl: './maestro-container.component.html',
  styleUrl: './maestro-container.component.scss',
  imports: [CommonModule, MaestroListComponent, MaestroCardComponent],
})
export class MaestroContainerComponent {
  selectedComposer: Composer | undefined = undefined;
  selectedComposerBio: string = '';
  composers: string[] = this.maestroDataService.getListOfComposers();

  constructor(private maestroDataService: MaestroDataService) {}

  onMaestroSelected(name: string) {
    console.log('onMaestroSelected -> ', name);
    this.selectedComposer = this.maestroDataService.getComposerData(name);
    this.selectedComposerBio = this.maestroDataService.getComposerBio(name);
    
  }
}
