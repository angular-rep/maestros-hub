import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-maestro-list',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './maestro-list.component.html',
  styleUrl: './maestro-list.component.scss',
})
export class MaestroListComponent {
  @Output('onComposer') onComposer = new EventEmitter<string>();

  clicked(composer: string) {
    console.log('clicked->', composer);
    this.onComposer.emit(composer);
  }
  @Input() composers: string[] = [];
}
